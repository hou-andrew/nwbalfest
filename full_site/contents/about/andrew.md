### Andrew Hou

A Seattle native for the past 10 years, Andrew is a dedicated Balboa dancer who also sits on the board of the Savoy Swing Club, a swing dancing non-profit organization. He loves eating noodles more than swing dancing. Andrew also loves Lego, is bad at computers, and has a car that frequently breaks down.
