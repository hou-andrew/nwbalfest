### Jennifer Lee

Traveling all around the world for Balboa, you're as likely to find Dr. Jennifer Lee dancing outside of Seattle as within it. Jennifer's proudest accomplishments include winning the Grand Forks 4-H Food Fair in North Dakota, playing Yum-Yum in The Mikado, being one of the "love" test subjects for an episode of Good Morning America, and starting her own practice as an Ophthalmologist.
