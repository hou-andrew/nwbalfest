### Sherman Ku

Sherman started dancing in Southern California, and holds the distinction of being the only organizer born in Asia. He was once nearly killed by an errant fireball from Andrew's car. In addition to being a tour guide for high school Science Day, Dr. Sherman Ku has also written papers on Friedreich's Ataxia Induced Pluripotent Stem Cells.
