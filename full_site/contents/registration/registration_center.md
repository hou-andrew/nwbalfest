Welcome to the __Northwest Balboa Festival 2015__, the Pacific Northwest's newest Balboa event! Our goal is to bring together dancers from all over to our little corner of the country, where we believe we have one of the best dance communities in the world. With an emphasis on social dancing, we're proud to bring you live music, afternoon dance parties, and a couple spins on a traditional event.

We believe that dance instruction is best when students are given opportunities to absorb what they've learned. With four hours of classes along with a guided practice session, we hope to bring you a unique and engaging learning experience. We also believe that dancing is best enjoyed with friends, which is why we've planned many ways to make sure you make new ones!

The Northwest Balboa Festival is a non-profit event. Any proceeds from the current year will be used for future events. In the event of the Northwest Balboa Festival being discontinued, all remaining proceeds will be donated to [Rhythm Seattle](http://www.rhythmseattle.org/), a program for bringing free swing dance education to high schools in Seattle.

Seattle is beautiful in the spring, and we hope to see all of you soon!
